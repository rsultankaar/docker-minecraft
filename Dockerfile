FROM adoptopenjdk/openjdk8:alpine-slim

ENV VERSION=1.12.2
ENV LATEST=14.23.5.2847
ENV FORGE_FILE=forge-$VERSION-$LATEST-installer.jar

RUN mkdir -p /opt/minecraft/ \
    && cd /opt/minecraft/ \
    && apk add --no-cache util-linux curl jq \
    && curl -O https://files.minecraftforge.net/maven/net/minecraftforge/forge/$VERSION-$LATEST/$FORGE_FILE \
    && ln -sf $FORGE_FILE /opt/minecraft/forge-installer.jar \
    && java -jar forge-installer.jar --installServer

WORKDIR /opt/minecraft

RUN ln -sf forge-$VERSION-$LATEST-universal.jar server.jar
RUN echo eula=true > eula.txt

COPY run.sh .
EXPOSE 25565

ENTRYPOINT sh -x ./run.sh
