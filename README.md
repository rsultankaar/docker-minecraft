# Docker Minecraft

## Build the image 

```
 ~$ docker build -t minecraft/jdk8 
```

### Start the container

You can use the following as a quickstart 
```
 ~$ docker run --rm \ 
    -v /var/run/docker.sock:/var/run/docker.sock \
    --memory=1G \
    --cpus=1.5 \ 
    -p 25565:25565 \
    -e XMX=1G \
    -e XMS=1G \
    --name mc-server minecraft/jdk8
```

Or parameterize what you wish

```
~$ docker run --rm \ 
    -v /var/run/docker.sock:/var/run/docker.sock \
    --memory=1G \
    --cpus=1.5 \ 
    -p 25565:25565 \
    -e ALLOW_NETHER=true \
    -e ALLOW_FLIGHT=false \
    -e BC_CONSOLE_TO_OPS=false \
    -e BC_RCON_TO_OPS=true \
    -e DIFFICULTY=easy \
    -e ENABLE_CMD_BLOCK=false \
    -e ENABLE_RCON=false \
    -e ENABLE_QUERY=false \
    -e ENFORCE_WHITELIST=false \
    -e FORCE_GAMEMODE=survival \
    -e FN_PERMISSION_LVL=survival \
    -e GAMEMODE=survival \
    -e GENERATE_STRUCTURES=true \
    -e GENERATOR_SETTINGS= \
    -e HARDCORE=false \
    -e LEVEL_NAME= \
    -e LEVEL_SEED=731F3C53-C3C1-4AA3-B614-C97F438914CB \
    -e MAX_BUILD_HEIGHT=256 \
    -e MAX_PLAYERS=20 \
    -e MAX_TICK_TIME=60000 \
    -e MOTD=256 \
    -e NET_CMP_THRESHOLD=256 \
    -e OP_PERMISSION_LEVEL=4 \
    -e ONLINE_MODE=false \
    -e PLAYER_IDLE_TIMEOUT=256 \
    -e PREVENT_PROXY_CON=false \
    -e PVP=256 \
    -e QUERY_PORT=25565 \
    -e PVP=256 \
    -e RESOURCE_PACK=http://download2267.mediafire.com/ufz6aeqbkiwg/e6wfozgw0jlkq3y/ModernArch+R32+%5B1.13+-+1.15%5D+%5B256x%5D.zip \
    -e RESOURCE_PACK_SHA1=0aa7d22d68045e17f567f0df5af3210cc9785c15 \
    -e MOD_URLS=https://media.forgecdn.net/files/2750/239/xray-1.12.2-1.6.0.jar,https://media.forgecdn.net/files/2684/33/PortalGun-1.12.2-7.1.0.jar,https://media.forgecdn.net/files/2801/262/iChunUtil-1.12.2-7.2.2.jar \
    -e RCON_PASSWORD=25565 \
    -e RCON_PORT=80 \
    -e SERVER_IP= \
    -e SERVER_PORT=25565 \
    -e SPAWN_NPCS=25565 \
    -e SPAWN_MONSTERS=25565 \
    -e SPAWN_PROTECTION=25565 \
    -e USE_NATIVE_TRANSPORT=25565 \
    -e VIEW_DISTANCE=10 \
    -e WWSNOOPER_ENABLED=10 \
    -e WHITE_LIST=10 \
    -e XMX=1G \
    -e XMS=1G \
    -e OPS=Skaar,jawsner62
    --name mc-server minecraft/jdk8
```
