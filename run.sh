#!/bin/sh

################################
######## SuperHero API #########
################################

API_TOKEN=10221538306587332
CHARACTER_ID=$(( $RANDOM % 731 + 10 ))
URL="https://superheroapi.com/api/$API_TOKEN/$CHARACTER_ID"
INSTANCE_NAME=`curl -s -L -XGET $URL | jq '.name | ascii_downcase' | sed -r "s#\"##g"`

################################
##### MINECRAFT OPTIONS ########
################################

#More info @ https://minecraft.gamepedia.com/Server.properties 

SERVER_PATH=/opt/minecraft
SERVER_PROPS="$SERVER_PATH/server.properties"

#Allow Nether
if [[ ! -z "$ALLOW_NETHER" ]]; then 
 echo "allow-nether=$ALLOW_NETHER" >> $SERVER_PROPS
else
 echo "allow-nether=true" >> $SERVER_PROPS
fi

#Allow Flight
if [[ ! -z "$ALLOW_FLIGHT" ]]; then 
 echo "allow-flight=$ALLOW_FLIGHT" >> $SERVER_PROPS
else
 echo "allow-flight=false" >> $SERVER_PROPS
fi

#Broadcast Console
if [[ ! -z "$BC_CONSOLE_TO_OPS" ]]; then 
 echo "broadcast-console-to-ops=$BC_CONSOLE_TO_OPS" >> $SERVER_PROPS
else
 echo "broadcast-console-to-ops=true" >> $SERVER_PROPS
fi

#Brodacast rcon
if [[ ! -z "$BC_RCON_TO_OPS" ]]; then 
 echo "broadcast-rcon-to-ops=$BC_RCON_TO_OPS" >> $SERVER_PROPS
else
 echo "broadcast-rcon-to-ops=true" >> $SERVER_PROPS
fi

#Brodacast rcon
if [[ ! -z "$DIFFICULTY" ]]; then 
 echo "difficulty=$DIFFICULTY" >> $SERVER_PROPS
else
 echo "difficulty=easy" >> $SERVER_PROPS
fi

#Enable command block
if [[ ! -z "$ENABLE_CMD_BLOCK" ]]; then 
 echo "enable-command-block=$ENABLE_CMD_BLOCK" >> $SERVER_PROPS
else
 echo "enable-command-block=false" >> $SERVER_PROPS
fi

#Enable command block
if [[ ! -z "$ENABLE_RCON" ]]; then 
 echo "enable-rcon=$ENABLE_RCON" >> $SERVER_PROPS
else
 echo "enable-rcon=false" >> $SERVER_PROPS
fi

#Enable Query
if [[ ! -z "$ENABLE_QUERY" ]]; then 
 echo "enable-query=$ENABLE_RCON" >> $SERVER_PROPS
else
 echo "enable-query=false" >> $SERVER_PROPS
fi

#Enforce whitelist
if [[ ! -z "$ENFORCE_WHITELIST" ]]; then 
 echo "enforce-whitelist=$ENABLE_QUERY" >> $SERVER_PROPS
else
 echo "enforce-whitelist=false" >> $SERVER_PROPS
fi

#Enforce whitelist
if [[ ! -z "$FORCE_GAMEMODE" ]]; then 
 echo "force-gamemode=$ENABLE_QUERY" >> $SERVER_PROPS
else
 echo "force-gamemode=false" >> $SERVER_PROPS
fi

#Function Permission Level
if [[ ! -z "$FN_PERMISSION_LVL" ]]; then 
 echo "function-permission-level=$FN_PERMISSION_LVL" >> $SERVER_PROPS
else
 echo "function-permission-level=2" >> $SERVER_PROPS
fi

#GameMode
if [[ ! -z "$GAMEMODE" ]]; then 
 echo "gamemode=$GAMEMODE" >> $SERVER_PROPS
else
 echo "gamemode=survival" >> $SERVER_PROPS
fi

#Generate Structures
if [[ ! -z "$GENERATE_STRUCTURES" ]]; then 
 echo "generate-structures=$GENERATE_STRUCTURES" >> $SERVER_PROPS
else
 echo "generate-structures=true" >> $SERVER_PROPS
fi

#Generator settings
echo "generator-settings=$GENERATOR_SETTINGS" >> $SERVER_PROPS

#Generator settings
if [[ ! -z "$HARDCORE" ]]; then 
 echo "hardcore=$HARDCORE" >> $SERVER_PROPS
else
 echo "hardcore=false" >> $SERVER_PROPS
fi

if [[ ! -z "$LEVEL_NAME" ]]; then 
  echo "level-name=$LEVEL_NAME" >> $SERVER_PROPS
else
  echo "level-name=$INSTANCE_NAME" >> $SERVER_PROPS
fi

#Level Seed
if [[ ! -z "$LEVEL_SEED" ]]; then 
 echo "level-seed=$LEVEL_SEED" >> $SERVER_PROPS
else
 echo "level-seed="$(uuidgen) >> $SERVER_PROPS
fi

#Max Height
if [[ ! -z "$MAX_BUILD_HEIGHT" ]]; then 
 echo "max-build-height=$MAX_BUILD_HEIGHT" >> $SERVER_PROPS
else
 echo "max-build-height=256" >> $SERVER_PROPS
fi

#Max-players
if [[ ! -z "$MAX_PLAYERS" ]]; then 
 echo "max-players=$MAX_PLAYERS" >> $SERVER_PROPS
else
 echo "max-players=20" >> $SERVER_PROPS
fi

#Max-tick-time
if [[ ! -z "$MAX_TICK_TIME" ]]; then 
 echo "max-tick-time=$MAX_TICK_TIME" >> $SERVER_PROPS
else
 echo "max-tick-time=60000" >> $SERVER_PROPS
fi

#Max-tick-time
if [[ ! -z "$MAX_WORLD_SIZE" ]]; then 
 echo "max-world-size=MAX_WORLD_SIZE" >> $SERVER_PROPS
else
 echo "max-world-size=29999984" >> $SERVER_PROPS
fi

#motd
if [[ ! -z "$MOTD" ]]; then 
 echo "motd=$MOTD" >> $SERVER_PROPS
else
 echo "motd=$INSTANCE_NAME" >> $SERVER_PROPS
fi

#Network-compression-threshold
if [[ ! -z "$NET_CMP_THRESHOLD" ]]; then 
 echo "network-compression-threshold=$NET_CMP_THRESHOLD" >> $SERVER_PROPS
else
 echo "network-compression-threshold=256" >> $SERVER_PROPS
fi

#Op-permission-level
if [[ ! -z "$OP_PERMISSION_LEVEL" ]]; then 
 echo "op-permission-level=$OP_PERMISSION_LEVEL" >> $SERVER_PROPS
else
 echo "op-permission-level=2" >> $SERVER_PROPS
fi

#Online mode
if [[ ! -z "$ONLINE_MODE" ]]; then 
 echo "online-mode=$ONLINE_MODE" >> $SERVER_PROPS
else
 echo "online-mode=false" >> $SERVER_PROPS
fi

#Player-idle-timeout
if [[ ! -z "$PLAYER_IDLE_TIMEOUT" ]]; then 
 echo "player-idle-timeout=$PLAYER_IDLE_TIMEOUT" >> $SERVER_PROPS
else
 echo "player-idle-timeout=0" >> $SERVER_PROPS
fi

#Prevent-proxy-connections
if [[ ! -z "$PREVENT_PROXY_CON" ]]; then 
 echo "prevent-proxy-connections=$PREVENT_PROXY_CON" >> $SERVER_PROPS
else
 echo "prevent-proxy-connections=false" >> $SERVER_PROPS
fi

#Pvp
if [[ ! -z "$PVP" ]]; then 
 echo "pvp=$PVP" >> $SERVER_PROPS
else
 echo "pvp=true" >> $SERVER_PROPS
fi

#Query Port
if [[ ! -z "$QUERY_PORT" ]]; then 
 echo "query.port=QUERY_PORT" >> $SERVER_PROPS
else
 echo "query.port=25565" >> $SERVER_PROPS
fi

#Query Port
echo "rcon.password=$RCON_PASSWORD" >> $SERVER_PROPS

#RCON PORT
if [[ ! -z "$RCON_PORT" ]]; then 
 echo "rcon.port=$RCON_PORT" >> $SERVER_PROPS
else
 echo "rcon.port=25575" >> $SERVER_PROPS
fi

if [[ ! -z "$RESOURCE_PACK" && ! -z "$RESOURCE_PACK_SHA1" ]];then
 encoded_url=`echo $RESOURCE_PACK | sed -r 's#(:|=)#\\\\\1#g'`
 echo "resource-pack=$encoded_url" >> $SERVER_PROPS
 echo "resource-pack-sha1=$RESOURCE_PACK_SHA1" >> $SERVER_PROPS
else
 echo "resource-pack=" >> $SERVER_PROPS
 echo "resource-pack-sha1=" >> $SERVER_PROPS
fi
#Server IP
echo "server-ip=$SERVER_IP" >> $SERVER_PROPS

#Server Port
if [[ ! -z "$SERVER_PORT" ]]; then 
 echo "server-port=$SERVER_PORT" >> $SERVER_PROPS
else
 echo "server-port=25565" >> $SERVER_PROPS
fi

#Spawn NPCs
if [[ ! -z "$SPAWN_NPCS" ]]; then 
 echo "spawn-npcs=$SPAWN_NPCS" >> $SERVER_PROPS
else
 echo "spawn-npcs=true" >> $SERVER_PROPS
fi

#Spawn NPCs
if [[ ! -z "$SPAWN_MONSTERS" ]]; then 
 echo "spawn-monsters=$SPAWN_MONSTERS" >> $SERVER_PROPS
else
 echo "sspawn-monsters=true" >> $SERVER_PROPS
fi

#Spawn protection
if [[ ! -z "$SPAWN_PROTECTION" ]]; then 
 echo "spawn-protection=$SPAWN_PROTECTION" >> $SERVER_PROPS
else
 echo "spawn-protection=16" >> $SERVER_PROPS
fi

#Use-native-transport
if [[ ! -z "$USE_NATIVE_TRANSPORT" ]]; then 
 echo "use-native-transport=$USE_NATIVE_TRANSPORT" >> $SERVER_PROPS
else
 echo "use-native-transport=true" >> $SERVER_PROPS
fi

#View Distance
if [[ ! -z "$VIEW_DISTANCE" ]]; then 
 echo "view-distance=$VIEW_DISTANCE" >> $SERVER_PROPS
else
 echo "view-distance=10" >> $SERVER_PROPS
fi

#White-list
if [[ ! -z "$WHITE_LIST" ]]; then 
 echo "white-list=$WHITE_LIST" >> $SERVER_PROPS
else
 echo "white-list=false" >> $SERVER_PROPS
fi

#WWSNOOPER_ENABLED
if [[ ! -z "$WWSNOOPER_ENABLED" ]]; then 
 echo "wwsnooper-enabled=$WWSNOOPER_ENABLED" >> $SERVER_PROPS
else
 echo "wwsnooper-enabled=true" >> $SERVER_PROPS
fi

### MODS ###
if [[ ! -z "$MOD_URLS" ]]; then
 mkdir $SERVER_PATH/mods/
 cd $SERVER_PATH/mods/
 echo $MOD_URLS | sed -r "s#,#\n#g" | xargs -I {} curl -O {}
 cd $SERVER_PATH/mods/..
 chmod -R 775 $SERVER_PATH/mods/
 chmod -R 775 $SERVER_PATH/mods/*
fi

# OPS Usernames
if [[ ! -z "$OPS" ]]; then
	echo $OPS | sed -r "s#,#\n#g" > ops.txt
fi

JAVA_OPTS="$JAVA_OPTS -DXmx=$XMX -DXms=$XMS"

java $JAVA_OPTS -jar server.jar nogui 

exit 0
